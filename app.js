
/**
 * Module dependencies.
 */

var express 	  = require('express');
var app			    = express();
var routes      = require('./routes');
var posts 		  = require('./routes/posts');
var http        = require('http');
var path        = require('path');
var bodyParser 	= require('body-parser');
var db 			    = require('./models');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(bodyParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get("/",routes.index);
app.get('/posts/:any', routes.index);
app.get('/posts/:any/edit', routes.index);
app.get("/posts", routes.index);


app.get('/api/posts', posts.read);
app.post('/api/posts', posts.create);
app.put('/api/posts/:id', posts.update);
app.delete('/api/posts/:id', posts.destroy);

var server = app.listen(3000, function() {
	console.log('Listening to port 3000');
})
db
  .sequelize
  .sync()
  .complete(function(err) {
    if (err) {
      throw err
    } else {
      http.createServer(app).listen(app.get('port'), function(){
        console.log('Express server listening on port ' + app.get('port'))
      })
    }
  })

