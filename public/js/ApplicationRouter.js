
window.APP = window.APP || {
  Router:{},
  Models :{},
  Collections:{},
  Views:{},
};
APP.Models.Post = Backbone.Model.extend({
    urlRoot: "/api/posts",
    defaults: {
        title: '',
        content: ''
    }
});
APP.Collections.Posts = Backbone.Collection.extend({
    model: APP.Models.Post,
    url: "/api/posts"
});

APP.Router = Backbone.Router.extend({

	initialize: function(el) {
		this.el = el;

		this.postListView = new PostListView(
			{
				template: Handlebars.compile(($("#post-list-tpl").html())),
				collection: APP.posts
			}
		);
		this.postSingleView = new PostSingleView(
			{
				template: Handlebars.compile(($("#post-single-tpl").html())),
			}
		);
		this.newPostView =  new PostFormView(
			{
				template: Handlebars.compile(($("#post-form-tpl").html()))
			}
		);
		this.editPostView = new PostFormView(
			{
				template: Handlebars.compile(($("#post-form-tpl").html()))
			}
		);
		this.notFoundView = new ContentView(			
			{
				template: Handlebars.compile(($("#not-found").html()))
			}
		);
	},

	routes: {
		"": "showPosts",
		"posts": "showPosts",
		"posts/new": "newPost",
		"posts/:id" : "showPost",
		"posts/:id/edit": "editPost",
		"*else": "notFound"
	},

	currentView: null,

	switchView: function(view,renderObject) {
		if (this.currentView) {
			// Detach the old view
			this.currentView.remove();
		}

		// Move the view element into the DOM (replacing the old content)
		this.el.html(view.el);

		// Render view after it is in the DOM (styles are applied)
		view.render(renderObject);

		this.currentView = view;
	},

	/*
	 * Change the active element in the topbar
	 */
	setActiveEntry: function(url) {
		// Unmark all entries
		$('li').removeClass('active');

		// Mark active entry
		$("li a[href='" + url + "']").parents('li').addClass('active');
	},

	showPosts: function() {
		this.switchView(this.postListView,APP.posts);
		this.setActiveEntry('#posts');
	},
	showPost: function(id) {
		post = APP.posts.get(id);
		this.switchView(this.postSingleView, post);
		this.setActiveEntry('#post-single-tpl');
	},
	newPost: function() {
		this.switchView(this.newPostView);
		this.setActiveEntry('#posts/new');
	},
	editPost: function(id) {
		post = APP.posts.get(id);
		this.switchView(this.editPostView,post);
		this.setActiveEntry('#post-form-tpl');
	},

	notFound: function() {
		this.switchView(this.notFoundView);
	}

});
