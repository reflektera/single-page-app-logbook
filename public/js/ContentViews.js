var ContentView = Backbone.View.extend({
	/*
	 * Initialize with the template-id
	 */
	initialize: function(options) {
		this.template = options.template;
	},

	/*
	 * Get the template content and render it into a new div-element
	 */
	render: function(renderObject) {
		var content = this.template();
		// this.$el.html(tpl({activities: this.coll2ection.toJSON()}));
		$(this.el).html(content);
		return this;
	}

});

var PostFormView = Backbone.View.extend({
	events: {
   		'submit form' : 'savePost',
   	},
	/*
	 * Initialize with the template-id
	 */
	initialize: function(options) {
		this.template = options.template;
	},

	savePost: function(e) {
		e.preventDefault();
		var title = e.target.elements[0].value;
    	var content = e.target.elements[1].value;
    	var id = e.target.elements[2].value;
 		if(id.length) {
        	var model = APP.posts.get(id);
        	model.save({
          		title:title,
          		content:content 
        	});
      	} else {
        	APP.posts.create({
            	title: title,
            	content: content
        	});
        }	
        router.navigate('/posts', { trigger: true });
	},

	/*
	 * Get the template content and render it into a new div-element
	 */
	render: function(renderObject) {
		var content = this.template();
		if(typeof(renderObject) != 'undefined') {
			content = this.template(renderObject.toJSON());
		}
		
		// this.$el.html(tpl({activities: this.collection.toJSON()}));
		$(this.el).html(content);
		return this;
	}

});
var PostListView = Backbone.View.extend({
	events: {
   		'click .post-link' : 'showPost',
   		'click .edit-link': 'editPost',
   		'click .delete-link': 'deletePost',
   	},
	/*
	 * Initialize with the template-id
	 */
	initialize: function(options) {
		this.template = options.template;
		this.collection = options.collection;
	},

	showPost: function(e) {
		e.preventDefault();
		postId = $(e.target).data("id");

		router.navigate('/posts/' + postId, { trigger: true });
	},
	editPost: function (e) {
		postId = $(e.target).data("id");
		router.navigate('/posts/' + postId + '/edit', { trigger: true });
	},
	deletePost: function (e) {
		var postId = $(e.target).data("id");
		var post = APP.posts.get(postId);
		post.destroy();
		router.switchView(this);

	},	
	render: function() {
		var content = this.template({posts:this.collection.toJSON()});
		$(this.el).html(content);
		return this;
	}

});
var PostSingleView = Backbone.View.extend({
	events: {
   		'click .post-link' : 'showPost',
   		'click .edit-link': 'editPost',
   		'click .delete-link': 'deletePost',
   	},
	/*
	 * Initialize with the template-id
	 */
	initialize: function(options) {
		this.template = options.template;
	},

	editPost: function (e) {
		postId = $(e.target).data("id");
		router.navigate('/posts/' + postId + '/edit', { trigger: true });
	},
	deletePost: function (e) {
		var postId = $(e.target).data("id");
		var post = APP.posts.get(postId);
		post.destroy();
		router.navigate('/posts', { trigger: true });
	},	

	/*
	 * Get the template content and render it into a new div-element
	 */
	render: function(renderObject) {
		var content = this.template(renderObject.toJSON());
		$(this.el).html(content);
		return this;
	}

});