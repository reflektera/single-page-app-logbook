var db = require("../models");
// Create
exports.create = function(req, res) {
	db.Post.create(req.body).complete(function(err, post) {
		res.json(post);
	});
}
// Read
exports.read = function(req, res){
 db.Post.findAll({order: "id DESC"}).success(function(posts) {
 	console.log(posts);
  res.json(posts);
 });
}
exports.getOne = function(req, res){
	var id = req.params.id;
	console.log(id);
	db.Post.find(id).success(function(posts) {
		res.json(posts);
	});
}
// Update
exports.update = function(req, res){
  db.Post.find(req.params.id).success(function(post) {
    post.updateAttributes(req.body).success(function(post) {
      res.json(post);
    })
  });
}
// Delete
exports.destroy = function(req, res){
  db.Post.find(req.params.id).success(function(post) {
    post.destroy().success(function() {
      res.status(200);
      res.end()
    })
  });
};