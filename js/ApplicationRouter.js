
window.APP = window.APP || {
  Router:{},
  Models :{},
  Collections:{},
  Views:{},
};
APP.Models.Post = Backbone.Model.extend({
    urlRoot: "/api/posts",
    defaults: {
        title: '',
        content: ''
    }
});
APP.Collections.Posts = Backbone.Collection.extend({
    model: APP.Models.Post,
    url: "http://localhost:3000/api/posts"
});



var ApplicationRouter = Backbone.Router.extend({

	initialize: function(el) {
		this.el = el;

		this.postListView = new ContentView({template: Handlebars.compile(($("#post-list-tpl").html()))});
		this.newPostView =  new ContentView({template: Handlebars.compile(($("#post-form-tpl").html()))});
		this.duisView = new ContentView({template: '#duis'});
		this.notFoundView = new ContentView({template: '#not-found'});
	},

	routes: {
		"": "showPosts",
		"posts": "showPosts",
		"posts/new": "newPost",
		"duis": "duis",
		"*else": "notFound"
	},

	currentView: null,

	switchView: function(view) {
		if (this.currentView) {
			// Detach the old view
			this.currentView.remove();
		}

		// Move the view element into the DOM (replacing the old content)
		this.el.html(view.el);

		// Render view after it is in the DOM (styles are applied)
		view.render();

		this.currentView = view;
	},

	/*
	 * Change the active element in the topbar
	 */
	setActiveEntry: function(url) {
		// Unmark all entries
		$('li').removeClass('active');

		// Mark active entry
		$("li a[href='" + url + "']").parents('li').addClass('active');
	},

	showPosts: function() {
		this.switchView(this.postListView);
		this.setActiveEntry('#post-list-tpl');
	},

	newPost: function() {
		this.switchView(this.newPostView);
		this.setActiveEntry('#post-form-tpl');
	},

	duis: function() {
		this.switchView(this.duisView);
		this.setActiveEntry('#duis');
	},

	notFound: function() {
		this.switchView(this.notFoundView);
	}

});
