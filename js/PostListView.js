/* *** VIEWS *** */
APP.Views.Post = Backbone.View.extend({
 
  tagName: "li",
  events: {
   'click .post a' : 'showPost',
   'click .delete' : 'deletePost',
   'click .edit' : 'editPost'
  },
  deletePost: function(e) {
    this.model.destroy();
    APP.router.navigate('posts', { trigger: true });
  }, 
  editPost: function(e) {
    e.preventDefault();
    APP.router.navigate('posts/'+this.model.get("id")+'/edit', { trigger: true });
  },
  showPost: function(e) {
    e.preventDefault();
    APP.router.navigate('posts/' + this.model.get("id"), { trigger: true });
  },
  render: function() {

    var html = this.template(this.model.toJSON());
    this.$el.html(html);
    return this;
  }
});
APP.Views.PostList = Backbone.View.extend({
    template: Handlebars.compile(($("#post-tpl").html())),
    tagName: "ul",
    attributes: {
      class: "list-unstyled"
    },
    initialize: function() {
      this.collection.on('sync remove', this.render, this);
    },
    addPostToList: function(post) {
      var postView = new APP.Views.Post();
      postView.model = post;
      this.$el.append(postView.render().el);
    },   
    render: function() {
      this.$el.empty();
      this.collection.each(this.addPostToList, this);
      return this.el;      
    }
});